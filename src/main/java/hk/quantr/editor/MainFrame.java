/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hk.quantr.editor;

import hk.quantr.editor.testingComponents.AndGate;
import hk.quantr.editor.testingComponents.NandGate;
import hk.quantr.editor.testingComponents.NorGate;
import hk.quantr.editor.testingComponents.NotGate;
import hk.quantr.editor.testingComponents.OrGate;
import hk.quantr.editor.testingComponents.OutputLed;
import hk.quantr.editor.testingComponents.XnorGate;
import hk.quantr.editor.testingComponents.XorGate;

/**
 *
 * @author peter
 */
public class MainFrame extends javax.swing.JFrame {

	/**
	 * Creates new form MainFrame
	 */
	public MainFrame() {
		initComponents();

		initEditor();
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
	 * Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        editor = new hk.quantr.editor.QuantrEditor();
        jToolBar1 = new javax.swing.JToolBar();
        zoomInButton = new javax.swing.JButton();
        zoomOutButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().add(editor, java.awt.BorderLayout.CENTER);

        jToolBar1.setRollover(true);

        zoomInButton.setText("+");
        zoomInButton.setFocusable(false);
        zoomInButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        zoomInButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        zoomInButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(zoomInButton);

        zoomOutButton.setText("-");
        zoomOutButton.setFocusable(false);
        zoomOutButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        zoomOutButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        zoomOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(zoomOutButton);

        getContentPane().add(jToolBar1, java.awt.BorderLayout.PAGE_START);

        jPanel1.add(jLabel1);

        getContentPane().add(jPanel1, java.awt.BorderLayout.LINE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void zoomInButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomInButtonActionPerformed
		QuantrEditor.zoomFactor += 0.3f;
		editor.repaint();
    }//GEN-LAST:event_zoomInButtonActionPerformed

    private void zoomOutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOutButtonActionPerformed
		QuantrEditor.zoomFactor -= 0.3f;
		editor.repaint();
    }//GEN-LAST:event_zoomOutButtonActionPerformed

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MainFrame().setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private hk.quantr.editor.QuantrEditor editor;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton zoomInButton;
    private javax.swing.JButton zoomOutButton;
    // End of variables declaration//GEN-END:variables

	private void initEditor() {
		editor.infoLabel = jLabel1;

//		EditorComponent e1 = new EditorComponent();
//		e1.name = "And gate";
//		e1.x = 10;
//		e1.y = 10;
//		e1.width = 5;
//		e1.height = 5;
//		editor.components.add(e1);
//
//		EditorComponent e2 = new EditorComponent();
//		e2.name = "OR gate";
//		e2.x = 15;
//		e2.y = 10;
//		e2.width = 5;
//		e2.height = 5;
//		e1.image = CommonLib.toBufferedImage(new ImageIcon(AndGate.class.getClassLoader().getResource("hk/quantr/editor/logic.png")).getImage());
//		editor.components.add(e2);
//
//		EditorComponent e3 = new EditorComponent();
//		e3.x = 10;
//		e3.y = 15;
//		e3.width = 5;
//		e3.height = 5;
//		e3.image = CommonLib.toBufferedImage(new ImageIcon(AndGate.class.getClassLoader().getResource("hk/quantr/editor/logic.png")).getImage());
//		editor.components.add(e3);
		editor.components.add(new OutputLed(20, 20));

		editor.components.add(new AndGate(10, 15));
		editor.components.add(new OrGate(15, 15));
		editor.components.add(new XorGate(20, 15));
		editor.components.add(new XnorGate(25, 15));
		editor.components.add(new NandGate(10, 25));
		editor.components.add(new NorGate(15, 25));
		editor.components.add(new NotGate(20, 25));
	}
}
