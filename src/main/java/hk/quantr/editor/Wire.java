/*
 * Copyright 2019 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.editor;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
class Wire extends JComponent implements IComponent {

	public int x1;
	public int y1;
	public int x2;
	public int y2;
	public float voltage;
	boolean visible = true;
	boolean isSelected;
	public int wireWidth = 3;

	public Wire(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	@Override
	public void setPosition(int x, int y) {
		if (y1 == y2) {
			int width = x2 - x1;

			this.x1 = x;
			this.y1 = y;
			this.x2 = x + width;
			this.y2 = y;
		} else {
			int height = y2 - y1;
			this.x1 = x;
			this.y1 = y;
			this.x2 = x;
			this.y2 = y + height;
		}
	}

	@Override
	public int getX() {
		return x1;
	}

	public int getY() {
		return y1;
	}

	public void draw(Graphics g, float voltage) {
		this.voltage = voltage;
		draw(g, -1, -1, voltage);
	}

	@Override
	public void draw(Graphics g, int drawX, int drawY, float voltage) {
		Graphics2D g2 = (Graphics2D) g;
		if (isSelected) {
			g.setColor(Color.orange);
		} else {
			g.setColor(Color.green.darker());
		}
		g2.setStroke(new BasicStroke(wireWidth));

		int componentX1 = x1 * QuantrEditor.gridSize;
		int componentY1 = y1 * QuantrEditor.gridSize;
		int componentX2 = x2 * QuantrEditor.gridSize;
		int componentY2 = y2 * QuantrEditor.gridSize;

		if (drawX == -1 || drawY == -1) {
			g.drawLine(componentX1, componentY1, componentX2, componentY2);
		} else {
			g.drawLine(drawX, drawY, drawX + (componentX2 - componentX1), drawY + (componentY2 - componentY1));
		}
	}

	@Override
	public String toString() {
		return "wire " + x1 + ", " + y1 + " > " + x2 + ", " + y2;
	}

	@Override
	public boolean checkMouseOverPoint(int mouseXSnapped, int mouseYSnapped) {
		if (x1 == x2) {
			if (mouseXSnapped == x1 && ((y1 <= mouseYSnapped && mouseYSnapped <= y2) || (y2 <= mouseYSnapped && mouseYSnapped <= y1))) {
				return true;
			}
		} else {
			if (mouseYSnapped == y1 && ((x1 <= mouseXSnapped && mouseXSnapped <= x2) || (x2 <= mouseXSnapped && mouseXSnapped <= x1))) {
				return true;
			}
		}
		return false;
	}
}
