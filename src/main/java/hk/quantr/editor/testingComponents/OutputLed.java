package hk.quantr.editor.testingComponents;

import hk.quantr.editor.Distance;
import hk.quantr.editor.EditorComponent;
import hk.quantr.editor.QuantrEditor;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author peter
 */
public class OutputLed extends EditorComponent {

	public OutputLed(int x, int y) {
		super.x = x;
		super.y = y;
		super.width = 2;
		super.height = 2;
		super.inputsDirection = Direction.WEST;
		super.inputs.add(new Distance(1, 10));

		super.outputsDirection = Direction.EAST;
		super.outputs.add(new Distance(1, 10));

		super.isShowBorder = false;
	}

	@Override
	public void draw(Graphics g, int drawX, int drawY, float voltage) {
		super.draw(g, drawX, drawY, voltage);
		int componentWidth = width * QuantrEditor.gridSize;
		int componentHeight = height * QuantrEditor.gridSize;

		if (voltage == 1) {
			g.setColor(Color.green);
		} else {
			g.setColor(Color.gray);
		}
		g.fillOval(drawX, drawY, componentWidth, componentHeight);
		super.drawInputsOutputs(g, drawX, drawY);
	}

}
