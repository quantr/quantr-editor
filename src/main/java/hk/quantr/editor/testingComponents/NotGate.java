package hk.quantr.editor.testingComponents;

import hk.quantr.editor.Distance;
import hk.quantr.editor.EditorComponent;
import hk.quantr.peterswing.CommonLib;
import java.awt.Color;
import javax.swing.ImageIcon;

/**
 *
 * @author peter
 */
public class NotGate extends EditorComponent {

	public NotGate(int x, int y) {
		super.x = x;
		super.y = y;
//		super.height = 4;
		super.name = "NOT";
		super.foreColor = Color.white;
		super.backgroundColor = Color.decode("#0b78ba");
		super.margin = 10;
		super.image = CommonLib.toBufferedImage(new ImageIcon(NotGate.class.getClassLoader().getResource("hk/quantr/editor/logicGates/not.png")).getImage());
		super.inputsDirection = Direction.WEST;
		super.inputs.add(new Distance(1, 10));
		super.inputs.add(new Distance(3, 10));

		super.outputsDirection = Direction.EAST;
		super.outputs.add(new Distance(2, 10));

		super.isShowBorder = false;
	}

//	@Override
//	public void draw(Graphics g, int drawX, int drawY, float voltage) {
//		Graphics2D g2 = (Graphics2D) g;
//		g2.setStroke(new BasicStroke(5));
//		g.setColor(Color.black);
//		g.drawLine(drawX, drawY + 2 * QuantrEditor.gridSize, drawX + 1 * QuantrEditor.gridSize, drawY + 2 * QuantrEditor.gridSize);
//		g.drawLine(drawX, drawY + 4 * QuantrEditor.gridSize, drawX + 1 * QuantrEditor.gridSize, drawY + 4 * QuantrEditor.gridSize);
//
//		super.draw(g, drawX, drawY, voltage);
//	}

}
