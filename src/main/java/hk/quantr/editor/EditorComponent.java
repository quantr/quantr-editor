package hk.quantr.editor;

import static hk.quantr.editor.QuantrEditor.selectedEditorComponent;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JComponent;

/**
 *
 * @author peter
 */
public class EditorComponent extends JComponent implements IComponent {

	public String name;
	public int x;
	public int y;
	public int width = 4;
	public int height = 4;
	public boolean isSelected;
	public int borderWidth = 2;
	public Color foreColor = Color.black;
	public Color backgroundColor = Color.pink;
	public Color borderColor = Color.red;
	public Color selectedBorderColor = Color.red;
	public int imageMargin = 0;
	public ArrayList<Distance> inputs = new ArrayList<Distance>();
	public ArrayList<Distance> outputs = new ArrayList<Distance>();
	public Direction inputsDirection = Direction.WEST;
	public Direction outputsDirection = Direction.EAST;
	public BufferedImage image;
	public Color inputsColor = Color.decode("#00aa00");
	public Color outputsColor = Color.decode("#aa0000");
	public boolean isShowBorder = true;
	public boolean isShowBorderStroke = false;
	public boolean isShowSelectedBorder = true;
	public float voltage;
	public int margin;

	public enum Direction {
		EAST,
		SOUTH,
		WEST,
		NORTH
	}

	@Override
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public void draw(Graphics g, float voltage) {
		this.voltage = voltage;
		int componentX = x * QuantrEditor.gridSize;
		int componentY = y * QuantrEditor.gridSize;
		draw(g, componentX, componentY, voltage);
	}

	@Override
	public void draw(Graphics g, int drawX, int drawY, float voltage) {
		Graphics2D g2 = (Graphics2D) g;

		this.voltage = voltage;
		AffineTransform affinetransform = new AffineTransform();
		FontRenderContext frc = new FontRenderContext(affinetransform, true, true);
		int fontWidth = 0;
		int fontHeight = 0;

		int componentWidth = width * QuantrEditor.gridSize;
		int componentHeight = height * QuantrEditor.gridSize;

		int componentX = drawX;
		int componentY = drawY;

		if (isShowBorder) {
			g.setColor(borderColor.brighter());
			g.drawRect(componentX, componentY, componentWidth, componentHeight);
		}

		if (isSelected) {
//			g.setColor(backgroundColor.brighter());
//			g.fillRect(componentX, componentY, componentWidth, componentHeight);

			if (isShowSelectedBorder) {
				g.setColor(selectedBorderColor);
				g.drawRect(componentX, componentY, componentWidth, componentHeight);
			}
		}
		g.setColor(backgroundColor);
		g2.fill(new RoundRectangle2D.Double(componentX, componentY, componentWidth, componentHeight, 20, 20));
		if (isShowBorderStroke) {
			g.setColor(backgroundColor.brighter());
			g2.setStroke(new BasicStroke(borderWidth));
			g2.draw(new RoundRectangle2D.Double(componentX + 2, componentY + 2, componentWidth - 4, componentHeight - 4, 20, 20));
		}

		int fontY = componentY;

		if (name != null) {
			imageMargin = 5;
			Font font = new Font("Arial", Font.PLAIN, 14);
			g.setFont(font);
			g.setColor(foreColor);
			fontWidth = (int) (font.getStringBounds(name, frc).getWidth());
			fontHeight = (int) (font.getStringBounds(name, frc).getHeight());
			fontY = componentY + fontHeight;
			g.drawString(name, componentX + (componentWidth - fontWidth) / 2, fontY);
		}

		if (image != null) {
			BufferedImage temp = null;
			int preferredImageWidth = componentWidth - margin;
			int preferredImageHeight = componentHeight - imageMargin - fontHeight - margin;
			if (temp == null) {
				temp = scaleImage(image, BufferedImage.TYPE_4BYTE_ABGR, preferredImageWidth, preferredImageHeight);
			}
			g.drawImage(temp, componentX + (componentWidth - temp.getWidth()) / 2, fontY + imageMargin, null);
		}

		drawInputsOutputs(g, componentX, componentY);
	}

	public static BufferedImage scaleImage(BufferedImage image, int imageType, int newWidth, int newHeight) {
		// Make sure the aspect ratio is maintained, so the image is not distorted
		double thumbRatio = (double) newWidth / (double) newHeight;
		int imageWidth = image.getWidth(null);
		int imageHeight = image.getHeight(null);
		double aspectRatio = (double) imageWidth / (double) imageHeight;

		if (thumbRatio < aspectRatio) {
			newHeight = (int) (newWidth / aspectRatio);
		} else {
			newWidth = (int) (newHeight * aspectRatio);
		}

		// Draw the scaled image
		Image tmp = image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
		BufferedImage resized = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();
		return resized;
	}

	public void drawInputsOutputs(Graphics g, int componentX, int componentY) {
		int componentWidth = width * QuantrEditor.gridSize;
		int componentHeight = height * QuantrEditor.gridSize;
		// inputs
		g.setColor(inputsColor);
		for (Distance d : inputs) {
			if (inputsDirection == Direction.NORTH) {
				g.fillOval(componentX + d.offset * QuantrEditor.gridSize - (d.size / 2), componentY - (d.size / 2), d.size, d.size);
			} else if (inputsDirection == Direction.SOUTH) {
				g.fillOval(componentX + d.offset * QuantrEditor.gridSize - (d.size / 2), componentY + componentHeight - (d.size / 2), d.size, d.size);
			} else if (inputsDirection == Direction.EAST) {
				g.fillOval(componentX + componentWidth - (d.size / 2), componentY + d.offset * QuantrEditor.gridSize - (d.size / 2), d.size, d.size);
			} else if (inputsDirection == Direction.WEST) {
				g.fillOval(componentX - (d.size / 2), componentY + d.offset * QuantrEditor.gridSize - (d.size / 2), d.size, d.size);
			}
		}

		// outputs
		g.setColor(outputsColor);
		for (Distance d : outputs) {
			if (outputsDirection == Direction.NORTH) {
				g.fillOval(componentX + d.offset * QuantrEditor.gridSize - (d.size / 2), componentY - (d.size / 2), d.size, d.size);
			} else if (outputsDirection == Direction.SOUTH) {
				g.fillOval(componentX + d.offset * QuantrEditor.gridSize - (d.size / 2), componentY + componentHeight - (d.size / 2), d.size, d.size);
			} else if (outputsDirection == Direction.EAST) {
				g.fillOval(componentX + componentWidth - (d.size / 2), componentY + d.offset * QuantrEditor.gridSize - (d.size / 2), d.size, d.size);
			} else if (outputsDirection == Direction.WEST) {
				g.fillOval(componentX - (d.size / 2), componentY + d.offset * QuantrEditor.gridSize - (d.size / 2), d.size, d.size);
			}
		}
	}

	@Override
	public boolean checkMouseOverPoint(int mouseXSnapped, int mouseYSnapped) {
		for (Distance d : inputs) {
			if (inputsDirection == Direction.NORTH) {
				if (mouseXSnapped == x + d.offset && mouseYSnapped == y) {
					return true;
				}
			} else if (inputsDirection == Direction.SOUTH) {
				if (mouseXSnapped == x + d.offset && mouseYSnapped == y + height) {
					return true;
				}
			} else if (inputsDirection == Direction.EAST) {
				if (mouseXSnapped == x + width && mouseYSnapped == y + d.offset) {
					return true;
				}
			} else if (inputsDirection == Direction.WEST) {
				if (mouseXSnapped == x && mouseYSnapped == y + d.offset) {
					return true;
				}
			}
		}
		for (Distance d : outputs) {
			if (outputsDirection == Direction.NORTH) {
				if (mouseXSnapped == x + d.offset && mouseYSnapped == y) {
					return true;
				}
			} else if (outputsDirection == Direction.SOUTH) {
				if (mouseXSnapped == x + d.offset && mouseYSnapped == y + height) {
					return true;
				}
			} else if (outputsDirection == Direction.EAST) {
				if (mouseXSnapped == x + width && mouseYSnapped == y + d.offset) {
					return true;
				}
			} else if (outputsDirection == Direction.WEST) {
				if (mouseXSnapped == x && mouseYSnapped == y + d.offset) {
					return true;
				}
			}
		}
		return false;
	}

}
